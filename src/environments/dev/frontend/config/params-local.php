<?php
return [
    'maxFileSize' => 1024 * 1024 * 2,
    'storagePath' => '@frontend/web/uploads/',
    'storageUrl' => '/uploads/',
];
