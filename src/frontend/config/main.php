<?php

$modules = require_once  __DIR__ . '/modules.php';

$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'language' => 'ru-RU',
    'bootstrap' => [
        'log',
        [
            'class' => \frontend\components\LanguageSelector::class,
        ]
       ],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
      'Image' => Intervention\Image\Facades\Image::class,

      'i18n' => [
          'translations' => [
              '*' => [
                  'class' => \yii\i18n\PhpMessageSource::class,
              ],
          ],
      ],

      'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => \common\models\User::class,
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'profile/<nickname:\w+>' => 'user/profile/view',
                'post/<id:\d+>' => 'post/default/view',
            ],
        ],
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'vk' => [
                    'class' => \yii\authclient\clients\VKontakte::class,
                    'clientId' => '7309793',
                    'clientSecret' => 'lGeG76BvCB1Erhf7QCql',
                    'scope' => 'email'
                ],
            ],
        ],
    ],
    'modules' => $modules,
    'params' => $params,
];
