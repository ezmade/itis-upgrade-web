<?php

/* @var $this yii\web\View */

use yii\web\JqueryAsset;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;


/* @var $this yii\web\View */
/**
 * @var \common\models\User [] $users
 */
$this->title = 'Feed';
?>
<?php if ($feedItems): ?>
    <?php foreach ($feedItems as $feedItem): ?>
        <?php /* @var $feedItem Feed */ ?>
        <?= $this->render('_part/_post', ['feedItem' => $feedItem, 'currentUser' => $currentUser]); ?>
    <?php endforeach; ?>
<?php else: ?>
    <div class="col-md-12">
        <div style="text-align: center"> No one posted yet!</div>
    </div>
<?php endif; ?>


<?php $this->registerJsFile('@web/js/likes.js', [
    'depends' => JqueryAsset::className(),
]);
$this->registerJsFile('@web/js/complaints.js', [
    'depends' => JqueryAsset::className(),
]);
