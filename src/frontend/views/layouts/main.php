<?php

/* @var $this \yii\web\View */

/* @var $content string */

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use frontend\assets\FontAwesomeAsset;
use frontend\assets\Bootstrap4Asset;

AppAsset::register($this);
FontAwesomeAsset::register($this);
Bootstrap4Asset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <?php $this->head() ?>
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KS855C6');</script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157940109-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-157940109-1');
    </script>

</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KS855C6"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<?php $this->beginBody() ?>

<div class="container container-heigh">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 image-header-block center-block" style="margin: 0px auto">
            <img src="/img/head.png" class="image-header hidden-xs">
        </div>
    </div>
    <?php
    $menuItems = [
        ['label' => Yii::t('menu', 'Newsfeed'), 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('menu', 'Signup'), 'url' => ['/user/default/signup']];
        $menuItems[] = ['label' => Yii::t('menu', 'Login'), 'url' => ['/user/default/login']];
    } else {
        $menuItems[] = ['label' => Yii::t('menu', 'My profile'), 'url' => ['/user/profile/view', 'nickname' => Yii::$app->user->identity->username]];
        $menuItems[] = ['label' => Yii::t('menu', 'Create post'), 'url' => ['/post/default/create']];
        $menuItems[] = ['label' => Yii::t('menu', 'Logout ({username})', ['username' => Yii::$app->user->identity->username]), 'url' => ['/user/default/logout']];
    }
    echo Nav::widget([
                         'options' => ['class' => 'nav nav-pills main-menu'],
                         'items' => $menuItems,
                     ]);
    ?>
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8 center-block ">
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
        <div class="col-md-2"></div>

        <div class="push"></div>
    </div>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<!-- Button trigger modal -->
