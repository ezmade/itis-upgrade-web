<?php
namespace frontend\modules\post\models\events;

use yii\base\Event;
use common\models\User;
use common\models\Post;

class PostCreatedEvent extends Event
{

    /**
     * @var User
     */
    public $user;

    /**
     *
     * @var Post
     */
    public $post;

    public function getUser(): User
    {
        return $this->user;
    }

    public function getPost(): Post
    {
        return $this->post;
    }

}
